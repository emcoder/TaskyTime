use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use app_dirs::*;

use day::Day;
use error::Error;
use serde_json;
use task::TaskItem;

use super::DaySave;

pub struct Storage {
  #[allow(dead_code)]
  root: PathBuf,
  data: PathBuf,
}

impl Storage {
  pub fn new(root: PathBuf, data: PathBuf) -> Storage {
    Storage { root, data }
  }

  pub fn init<P: Into<PathBuf>>(root: Option<P>) -> Result<Storage, Error> {
    if let Some(root) = root {
      let root = root.into();
      let mut data: PathBuf = root.clone();
      data.push("data");
      fs::create_dir_all(&data)?;

      return Ok(Storage::new(root, data));
    }

    const APP_INFO: AppInfo = AppInfo {
      name: "TaskyTime",
      author: "fludardes",
    };

    let root = app_root(AppDataType::UserConfig, &APP_INFO)
      .map_err(|err| Error::with_reason("Failed to get config directory", &err))?;

    let data = app_dir(AppDataType::UserData, &APP_INFO, "data")
      .map_err(|err| Error::with_reason("Failed to get data directory", &err))?;

    debug!(
      "Storage initalized with config directory {:?} and data {:?}",
      root, data
    );

    Ok(Storage::new(root, data))
  }

  #[allow(dead_code)]
  pub fn root(&self) -> &Path {
    self.root.as_path()
  }

  fn day_path(&self, day_name: &str) -> PathBuf {
    let file_name = format!("{}.json", day_name);
    self.data().join(file_name)
  }

  pub fn data(&self) -> &Path {
    self.data.as_path()
  }

  pub fn save(&self, day: &Day) -> Result<(), Error> {
    let path = self.day_path(day.name().as_str());
    let path_str = path
      .to_str()
      .ok_or(Error::new("Path to string conversion failed"))?;

    debug!("Save day to file {}", path_str); // FIXME: Remove unwrap

    let day_storage = DaySave::from(day);

    let day_json = serde_json::to_string(&day_storage)
      .map_err(|err| Error::with_reason("Day JSON error", &err))?;

    let mut file =
      File::create(&path).expect(&format!("Failed to open file {} to write Day", path_str));

    file.write(day_json.as_bytes())?;

    Ok(())
  }

  pub fn load<S: Into<String>>(&self, name: S) -> Result<Day, Error> {
    let name = name.into();
    info!("Loading day {}...", name);

    let path = self.day_path(&name);

    debug!("Day path {}", path.to_str().unwrap());

    if !path.exists() {
      let err = Error::new(format!("Day {} not found", name));
      return Err(err);
    }

    let file = File::open(path)?;
    let mut buf_reader = BufReader::new(file);

    let mut day_json = String::new();

    buf_reader.read_to_string(&mut day_json)?;

    let data: DaySave = serde_json::from_str(&day_json)
      .map_err(|err| Error::with_reason(format!("Failed to parse day {}", &name), &err))?;

    let tasks: Vec<TaskItem> = data
      .tasks()
      .into_iter()
      .map(|task| Rc::new(task.into()))
      .collect();

    Ok(Day::new(name, tasks))
  }

  pub fn exists(&self, name: &str) -> bool {
    self.day_path(name).exists()
  }
}
