use std::rc::Rc;

use task::Task;

#[derive(Serialize, Deserialize, Debug)]
pub struct TaskSave {
  name: String,
  time: u64,
}

impl TaskSave {
  pub fn name(&self) -> &str {
    &self.name
  }

  pub fn time(&self) -> u64 {
    self.time
  }
}

impl From<Rc<Task>> for TaskSave {
  fn from(task: Rc<Task>) -> TaskSave {
    TaskSave {
      // XXX: Unneeded allocation?
      name: task.name().to_owned(),
      time: task.time().as_secs(),
    }
  }
}
