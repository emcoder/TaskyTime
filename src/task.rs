use std::cell::{Ref, RefCell};
use std::rc::Rc;

pub type TaskItem = Rc<Task>;

use error::Error;
use std::time::Duration;
use storage::TaskSave;

pub struct Task {
  name: RefCell<String>,
  // Time in seconds spent on task
  time: RefCell<Duration>,
}

impl Task {
  pub fn new<S: Into<String>>(name: S) -> Task {
    let name = name.into();
    Task {
      name: RefCell::new(name),
      time: RefCell::new(Duration::from_secs(0)),
    }
  }

  pub fn time(&self) -> Ref<Duration> {
    self.time.borrow()
  }

  pub fn name(&self) -> Ref<String> {
    self.name.borrow()
  }

  pub fn set_name<S: Into<String>>(&self, name: S) {
    self.name.replace(name.into());
  }

  pub fn set_time(&self, time: Duration) {
    self.time.replace(time);
  }

  pub fn track(&self, time: Duration) -> Result<(), Error> {
    let dur = *self.time.borrow();
    self.time.replace(
      dur
        .checked_add(time)
        .ok_or(Error::new("Failed to track add time"))?,
    );
    Ok(())
  }
}

impl<'a> From<&'a TaskSave> for Task {
  fn from(task: &TaskSave) -> Task {
    Task {
      name: RefCell::new(task.name().to_owned()),
      time: RefCell::new(Duration::from_secs(task.time())),
    }
  }
}
