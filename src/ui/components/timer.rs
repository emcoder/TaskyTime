use std::cell::{Cell, RefCell};
use std::rc::Rc;
use std::time::Duration;

use gtk;
use gtk::prelude::*;
use gtk::{Builder, Button, Dialog, Entry, Image, Label};

use ui::builder_object;
use error::Error;
use task::Task;
use utils;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TimerStatus {
  Run,
  Pause,
  Stop
}

#[derive(Clone)]
pub struct Timer {
  status: Rc<Cell<TimerStatus>>,
  time: Rc<Cell<Duration>>,
  on_tick: Rc<RefCell<Box<Fn(Duration, TimerStatus) -> () + 'static>>>,

  button_start_pause: Button,
  button_stop: Button,

  label_timer: Label,
  image_timer_pause: Image,
  image_timer_start: Image,
}

impl Timer {
  pub fn build(builder: &Builder) -> Result<Timer, Error> {
    let timer = Timer {
      status: Rc::new(Cell::new(TimerStatus::Stop)),
      time: Rc::new(Cell::new(Duration::from_secs(0))),

      on_tick: Rc::new(RefCell::new(Box::new(|_, _| {}))),

      button_start_pause: builder_object(&builder, "button_timer_start_pause")?,
      button_stop: builder_object(&builder, "button_timer_stop")?,

      label_timer: builder_object(&builder, "label_timer")?,
      image_timer_pause: builder_object(&builder, "image_timer_pause")?,
      image_timer_start: builder_object(&builder, "image_timer_start")?,
    };

    Ok(timer)
  }

  pub fn connect_tick<F>(&self, func: F) -> ()
    where F: Fn(Duration, TimerStatus) -> () + 'static {

    trace!("Connect timer tick action...");
    self.on_tick.replace(Box::new(func));

  }

  pub fn set_sensitive(&self, sensitive: bool) {
    self.button_start_pause.set_sensitive(sensitive);
    self.label_timer.set_sensitive(sensitive);
  }

  fn set_status(&self, status: TimerStatus) {
    trace!("Set timer status to {:?}", status);
    self.status.set(status);
  }

  fn on_start(&self) {
    info!("Start timer");
    self.set_status(TimerStatus::Run);
    self.button_stop.set_sensitive(true);
    self.button_start_pause.set_image(Some(&self.image_timer_pause));

    self.run_timer();
  }

  pub fn status(&self) -> TimerStatus {
    self.status.get()
  }

  fn run_timer(&self) {
    let timer = self.clone();
    gtk::timeout_add_seconds(1u32, move || {
      let status = timer.status();
      let time = timer.time();
      let action = timer.on_tick.borrow();

      if status == TimerStatus::Run {
        let new_time = Duration::from_secs(time.as_secs() + 1);
        timer.set_time(new_time);
        timer.show_time();
        action(new_time, status);
        Continue(true)
      } else {
        action(time, status);
        Continue(false)
      }
    });
  }

  pub fn time(&self) -> Duration {
    self.time.get()
  }

  pub fn set_time(&self, time: Duration) {
    self.time.set(time)
  }

  fn show_time(&self) {
    let time_text = utils::format_time_pretty(&self.time());
    self.label_timer.set_text(time_text.as_str());
  }

  fn on_pause(&self) {
    info!("Pause timer");
    self.set_status(TimerStatus::Pause);
    self.button_start_pause.set_image(Some(&self.image_timer_start));
  }

  pub fn on_stop(&self) {
    info!("Stop timer");
    self.set_status(TimerStatus::Stop);
    self.button_stop.set_sensitive(false);
    self.button_start_pause.set_image(Some(&self.image_timer_start));
    self.time.replace(Duration::from_secs(0));
    self.show_time();
  }

  fn on_start_pause(&self) {
    let status = self.status();

    // XXX: Hack for bug with Stop status is not set
    if !self.button_stop.get_sensitive() && status == TimerStatus::Run {
      self.set_status(TimerStatus::Stop);
    }

    match self.status() {
      TimerStatus::Run => self.on_pause(),
      _ => self.on_start()
    }
  }

  pub fn run(&self) {
    {
      let timer = self.clone();
      self
        .button_start_pause
        .connect_clicked(move |_| timer.on_start_pause());
    }

    {
      let timer = self.clone();
      self
        .button_stop
        .connect_clicked(move |_| timer.on_stop());
    }
  }
}