use std::cell::Cell;
use std::rc::Rc;
use std::time::Duration;

use gtk::prelude::*;
use gtk::{Builder, Button, Dialog, Entry};

use super::builder_object;
use error::Error;
use task::Task;
use utils;

type Mode = Rc<Cell<bool>>;

#[derive(Clone)]
pub struct AddTaskDialog {
  edit_mode: Mode,
  dialog: Dialog,
  button_task_add: Button,
  entry_task_name: Entry,
  entry_task_time: Entry,
  button_cancel: Button,
}

impl AddTaskDialog {
  pub fn build() -> Result<AddTaskDialog, Error> {
    trace!("Add task dialog build run...");

    let glade_src = include_str!("../../resources/ui/add_task_dialog.glade");
    let builder = Builder::new_from_string(&glade_src);

    debug!("Ui builded");

    let dialog: Dialog = builder_object(&builder, "dialog_task_add")?;

    {
      dialog.connect_delete_event(move |dlg, _| {
        dlg.hide_on_delete();
        Inhibit(true)
      });
    }

    let button_cancel: Button = builder_object(&builder, "button_cancel")?;

    {
      let dialog = dialog.clone();
      button_cancel.connect_clicked(move |_| {
        dialog.hide();
      });
    }

    let add_task_dialog = Self {
      edit_mode: Rc::new(Cell::new(false)),
      dialog,
      button_task_add: builder_object(&builder, "button_task_add")?,
      button_cancel,
      entry_task_name: builder_object(&builder, "entry_task_name")?,
      entry_task_time: builder_object(&builder, "entry_task_time")?,
    };

    Ok(add_task_dialog)
  }

  pub fn show(&self) {
    trace!("Show AddTaskDialog in add mode...");
    self.edit_mode.set(false);
    self.dialog.set_title(t!("Add task").as_str());
    self.button_task_add.set_label(t!("Add").as_str());
    self.entry_task_name.set_text("");
    self.entry_task_time.set_text("");
    self.dialog.show_all();
  }

  pub fn show_edit(&self, task: &Task) {
    trace!("Show AddTaskDialog in edit mode...");
    self.edit_mode.set(true);
    self.dialog.set_title(t!("Edit task").as_str());
    self.button_task_add.set_label(t!("Save").as_str());
    self.entry_task_name.set_text(task.name().as_str());
    self
      .entry_task_time
      .set_text(utils::format_time(&task.time()).as_str());
    self.dialog.show_all();
  }

  pub fn edit_mode(&self) -> bool {
    self.edit_mode.get()
  }

  pub fn on_confirm<F: Fn(String, Duration) + 'static>(&self, op: F) {
    let dialog = self.dialog.clone();
    let entry_task_name = self.entry_task_name.clone();
    let entry_task_time = self.entry_task_time.clone();
    self.button_task_add.connect_clicked(move |_| {
      let name = if let Some(name) = entry_task_name.get_text() {
        name
      } else {
        trace!("Can't get edited task name");
        return ();
      };

      let time = if let Some(time) = entry_task_time.get_text() {
        time
      } else {
        trace!("No edited task time found");
        String::new()
      };

      let time = if let Ok(time) = utils::parse_time(time) {
        time
      } else {
        warn!("Try to set invalid time for task");
        return;
      };

      dialog.hide();

      op(name, time);
    });
  }
}
