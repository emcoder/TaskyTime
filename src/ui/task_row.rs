use gtk::prelude::*;

use gtk;
use gtk::{Label, Orientation, Separator};

use super::style;
use task::Task;
use utils;

#[derive(Clone)]
pub struct TaskRow {
  widget: gtk::Box,
  task_name_label: Label,
  task_time_label: Label,
}

impl TaskRow {
  pub fn new(task: &Task) -> Self {
    let task_box = gtk::Box::new(Orientation::Horizontal, 0);
    let task_name_label = task_label(task.name().as_str());
    task_name_label.set_hexpand(true);
    task_box.add(&task_name_label);

    {
      let sep = Separator::new(Orientation::Vertical);
      task_box.add(&sep);
    }

    let task_time_label = task_label(utils::format_time(&task.time()).as_str());
    task_box.add(&task_time_label);

    let wrapper = gtk::Box::new(Orientation::Vertical, 0);
    wrapper.add(&task_box);
    wrapper.show_all();

    Self {
      widget: wrapper,
      task_name_label,
      task_time_label,
    }
  }

  pub fn widget(&self) -> gtk::Box {
    self.widget.clone()
  }

  pub fn show_task(&self, task: &Task) {
    trace!("Task item showing task...");
    self.task_name_label.set_text(task.name().as_str());
    self
      .task_time_label
      .set_text(utils::format_time(&task.time()).as_str())
  }
}

fn task_label(name: &str) -> Label {
  let label = Label::new(Some(name));
  set_widget_margin(&label, style::MARGIN);
  label
}

#[allow(dead_code)]
fn set_widget_margin<W: WidgetExt>(widget: &W, margin: i32) {
  widget.set_margin_start(margin);
  widget.set_margin_end(margin);
  widget.set_margin_top(margin);
  widget.set_margin_bottom(margin);
}
