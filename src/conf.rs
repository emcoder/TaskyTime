pub const VERSION: &'static str = env!("CARGO_PKG_VERSION");
pub const AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");
pub const APP_ID: &'static str = "com.gitlab.fludardes.TaskyTime";
