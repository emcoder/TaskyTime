#[macro_use]
extern crate log;
extern crate chrono;
extern crate clap;
extern crate logme;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate app_dirs;
extern crate gettextrs;
extern crate regex;
extern crate serde_json;
extern crate strfmt;
#[macro_use]
extern crate lazy_static;
extern crate gio;
extern crate gtk;

pub use app::App;
pub use day::Day;
pub use error::Error;
pub use storage::Storage;
pub use task::Task;

#[macro_use]
pub mod utils; // Public for tests
mod actions;
pub mod app;
mod cli;
mod conf;
mod day;
mod error;
//mod logic;
mod storage;
mod task;
mod ui;
