use std::collections::HashMap;
use std::env;
use std::fmt::Display;
use std::path::PathBuf;

use gettextrs::*;
use regex::Regex;
use strfmt::strfmt;

use error::Error;

lazy_static! {
  static ref PLACEHOLDER_REGEX: Regex =
    Regex::new(r"\{\}").expect("Failed to compile placeholder regex");
}

#[macro_export]
macro_rules! t {
  ( $a:expr $(,$b:expr)* ) => {{
    use std::collections::HashMap;
    use utils::translate;

    #[allow(unused_mut)]
    #[allow(unused_variables)]
    let mut i = 0;
    #[allow(unused_mut)]
    let mut args: HashMap<String, String> = HashMap::new();
    $(
      i += 1;
      args.insert(i.to_string(), $b.to_string());
    )*

    translate($a, args)
  }};
}

pub fn translate<S: Into<String>, T: Display>(format: S, args: HashMap<String, T>) -> String {
  let mut locale_string = gettext(format.into());

  let count: u8 = {
    PLACEHOLDER_REGEX
      .captures_iter(locale_string.as_str())
      .count() as u8
  };

  for i in 1..=count {
    let rep = format!("{{{}}}", i);
    locale_string = PLACEHOLDER_REGEX
      .replace(&locale_string, rep.as_str())
      .into_owned();
  }

  if args.len() > 0 {
    strfmt(locale_string.as_str(), &args).expect("Format translate failed")
  } else {
    locale_string
  }
}

pub fn localization() -> Result<(), Error> {
  // Empty string fallbacks to environment variables
  setlocale(LocaleCategory::LcAll, "");
  let executable = env::current_exe();

  let mut path = if let Ok(executable) = executable {
    if let Some(parent) = executable.parent() {
      parent.to_owned()
    } else {
      PathBuf::new()
    }
  } else {
    PathBuf::new()
  };

  path.push("locale");

  if !path.exists() {
    path = PathBuf::from("locale");
  }

  let path_str = path
    .to_str()
    .expect("Failed to parse current executable path as string");

  bindtextdomain("TaskyTime", path_str);
  textdomain("TaskyTime");
  Ok(())
}
