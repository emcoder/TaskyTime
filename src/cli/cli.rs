use super::build;
use clap::ArgMatches;
use utils;
use Day;

pub struct Cli<'a> {
  args: ArgMatches<'a>,
}

impl<'a> Cli<'a> {
  pub fn init(args: Option<&[&str]>) -> Self {
    let args = build(args);
    Self { args }
  }

  pub fn args(&'a self) -> &'a ArgMatches {
    &self.args
  }

  pub fn verbosity(&self) -> u64 {
    self.args.occurrences_of("verbose")
  }

  pub fn cli_mode(&self) -> bool {
    self.args.is_present("cli") || self.args.is_present("task")
  }

  pub fn show_day(&self, day: &Day) {
    println!("DAY {} TASKS:", day.name());

    for (i, task) in day.tasks().iter().enumerate() {
      println!(
        " {} | {} | {}",
        i + 1,
        utils::format_time(&task.time()),
        task.name()
      );
    }
  }
}
