use std::rc::Rc;

use cli::Cli;
use day::Day;
use error::Error;
use storage::Storage;
use ui::Ui;
use utils;

pub struct App {
  day: Day,
  storage: Storage,
}

impl App {
  pub fn new(storage: Storage) -> Self {
    // XXX: Dirty trick? Just wanted to avoid unneded Option
    let day = Day::new("empty", Vec::new());
    Self { day, storage }
  }

  pub fn day(&self) -> &Day {
    &self.day
  }

  pub fn storage(&self) -> &Storage {
    &self.storage
  }
}

pub fn run(args: Option<&[&str]>, root: Option<&str>) -> Result<(), Error> {
  utils::localization()?;

  let cli = Cli::init(args);

  utils::log_init(cli.verbosity()).unwrap_or_else(|err| {
    eprintln!("Failed to initialize logger: {}", err);
  });

  let storage = Storage::init(root)?;
  let app = App::new(storage);

  {
    let day = {
      let date = cli.args().value_of("day");
      app.init_day(date)?
    };

    info!("Day {} initialized", day.name());
  }

  //let day = Rc::new(day);

  //let app = Rc::new(App::new(day, Rc::new(storage)));

  //app.init_day();

  if cli.cli_mode() {
    cli.process(&app)?;
    return Ok(());
  }

  let app = Rc::new(app);

  let ui = Ui::build()?;
  ui.run(Rc::clone(&app))?;

  Ok(())
}
