use std::rc::Rc;

use clap::ArgMatches;

use app::App;
use day::Day;
use error::Error;
use storage::Storage;
use task::Task;
use utils;

pub fn init_day(name: Option<&str>, storage: &Storage) -> Result<Day, Error> {
  let name = if let Some(name) = name {
    name.to_owned()
  } else {
    utils::today_name()
  };

  let day = if storage.exists(&name) {
    storage.load(name)?
  } else {
    Day::new(name, Vec::new())
  };

  Ok(day)
}

pub fn task_track(id: &str, time: &str, day: &Day) -> Result<(), Error> {
  let task = day
    .task_by_id(id)
    .ok_or(Error::new(format!("Day with id {} not found", id)))?;
  println!("Tracking {} for task {}", time, task.name());
  let time = utils::parse_time(time)?;
  task.track(time)?;
  println!("Task time now {}", utils::format_time(&task.time()));
  Ok(())
}

pub fn task_add<S: Into<String>>(name: S, day: &Day) -> Result<(), Error> {
  let name = name.into();
  let task = Task::new(name);
  day.add_task(task);
  Ok(())
}

// Remove task by its number in list or by name
fn task_remove<S: Into<String>>(id: S, day: &Day) -> Result<(), Error> {
  let id = id.into();

  // If text or numeric id
  let task_id = match id.parse::<usize>() {
    Ok(num) => Some(num),
    Err(_) => day.task_number_by_name(&id),
  };

  let task_id = task_id.ok_or(Error::new(format!("Task with id \"{}\" not found", id)))?;

  if day.tasks().get(task_id - 1).is_none() {
    return Err(Error::new("Remove task item with specified id not found"));
  }

  day.remove_task(task_id);

  Ok(())
}

fn task_update<S: Into<String>>(
  id: S,
  name: Option<S>,
  track: Option<S>,
  day: &Day,
) -> Result<(), Error> {
  let id = id.into();

  if name.is_none() && track.is_none() {
    return Err(Error::new(
      "Please, specify parameter to update (--name or --track time)",
    ));
  }

  let task = day
    .task_by_id(id.as_str())
    .ok_or(Error::new(format!("Task with id {} not found", id)))?;

  if let Some(name) = name {
    let name = name.into();
    println!("Task \"{}\" name change to \"{}\"", task.name(), name);
    task.set_name(name);
  }

  if let Some(track) = track {
    let track = track.into();
    let time = utils::parse_time(track)?;
    task.set_time(time);
    println!(
      "Task \"{}\" tracked time set to {}",
      task.name(),
      utils::format_time(&task.time())
    );
  }

  Ok(())
}

pub fn tasks_process(args: &ArgMatches, app: &App) -> Result<(), Error> {
  let day = app.day();
  let storage = app.storage();

  if args.is_present("list") {
    day.show();
    return Ok(());
  }

  let name = args
    .value_of("name")
    .ok_or(Error::new("Task name missing"))?;

  if args.is_present("add") {
    task_add(name, &day)?;
  } else if args.is_present("remove") {
    task_remove(name, &day)?;
  } else if args.is_present("update") {
    task_update(
      name,
      args.value_of("new_name"),
      args.value_of("track"),
      &day,
    )?;
  } else if args.is_present("track") {
    let track = args
      .value_of("track")
      .ok_or(Error::new("Track time required"))?;

    task_track(name, track, &day)?;
  } else {
    println!("Show task: {}", name);
    return Ok(());
  }

  storage
    .save(&day)
    .map_err(|err| Error::with_reason("Data save error", &err))?;
  day.show();

  Ok(())
}

#[cfg(test)]
mod tests {
  #[allow(unused_imports)]
  use super::{init_day, task_track};
  use std::env;
  #[allow(unused_imports)]
  use std::time::Duration;
  use storage::Storage;
  #[allow(unused_imports)]
  use task::Task;

  #[test]
  fn init_day_test() {
    let storage = Storage::new(env::temp_dir(), env::temp_dir());
    let day = init_day(Some("test"), &storage).expect("Failed to get day");
    assert_eq!(day.name(), "test");
  }

  #[test]
  fn track_time_test() {
    let storage = Storage::new(env::temp_dir(), env::temp_dir());
    let mut day = init_day(Some("test"), &storage).unwrap();
    let task = Task::new("Test Task");
    day.add_task(task);
    task_track("1", "00:20:42", &mut day).unwrap();
    assert_eq!(day.task_by_number(1).unwrap().time().as_secs(), 1242);
  }
}
