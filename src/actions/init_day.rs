use utils;
use App;
use Day;
use Error;

impl App {
  // Load or create day — with current date or specified
  pub fn init_day<S: Into<String>>(&self, date: Option<S>) -> Result<&Day, Error> {
    let date = if let Some(date) = date {
      date.into()
    } else {
      utils::today_name()
    };

    let storage = self.storage();

    let day = if storage.exists(date.as_str()) {
      storage.load(date)?
    } else {
      Day::new(date, Vec::new())
    };

    self.day().replace(day);

    Ok(self.day())
  }
}

#[cfg(test)]
mod tests {
  use std::path::PathBuf;

  use utils::today_name;
  use App;
  use Storage;
  use Task;

  use utils::test;

  #[test]
  fn empty_today_init_test() {
    let storage = Storage::new(PathBuf::from("/tmp"), PathBuf::from("/tmp"));
    let app = App::new(storage);

    let day = app
      .init_day::<String>(None)
      .expect("Failed to init empty day");
    assert_eq!(today_name(), day.name().as_str());
  }

  #[test]
  fn exist_day_init_test() {
    test::run_temp_root(|root| {
      // Create empty day and save task to it
      {
        let storage = Storage::init(Some(root.as_path())).expect("Failed to initialize storage");
        let app = App::new(storage);
        let day = app
          .init_day(Some("1997-05-26"))
          .expect("Failed to init empty day");
        let task = Task::new("Happy Birthday");
        day.add_task(task);
        app.storage().save(&day).expect("Failed to save day");
      }
      // Read existing day
      {
        let storage = Storage::init(Some(root.as_path())).expect("Failed to initialize storage");
        let app = App::new(storage);
        let day = app
          .init_day(Some("1997-05-26"))
          .expect("Failed to init existing day");
        let task = day.task_by_number(1).expect("Failed to get task");
        assert_eq!(task.name().as_str(), "Happy Birthday");
      }
    });
  }
}
