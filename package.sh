#!/usr/bin/env bash

# BUILD ARCHIVE WITH BINARY AND RESOURCES
# TODO: Replace this script with Makefile or something like Makefile

ROOT=`pwd`
BIN_NAME="tasky-time"
BUILD_DIR="$ROOT/target/release"
MANIFEST='Cargo.toml'
BINARY="$BUILD_DIR/$BIN_NAME"
RELEASE_DIR="$ROOT/releases"
LOCALE_DIR="$ROOT/locale"

# BUILD RELEASE
cargo build --release

if [ $? != 0 ]; then
  echo "ERROR: Release build failed!"
  exit 1
fi

# CHECK FILES
if [ ! -d "$BUILD_DIR" ]; then
  echo "ERROR: Release directory $BUILD_DIR not found!"
  exit 1
fi

if [ ! -f "$MANIFEST" ]; then
  echo "ERROR: $MANIFEST not found, can't get version!"
  exit 1
fi

if [ ! -f "$BINARY" ]; then
  echo "ERROR: Release binary $BINARY not found!"
  exit 1
fi

if [ ! -d "$LOCALE_DIR" ]; then
  echo "ERROR: Locale resources $LOCALE_DIR not found!"
  exit 1
fi

echo "Found binary $BINARY..."

if [ ! -d "$RELEASE_DIR" ]; then
  echo "Create release directory $RELEASE_DIR..."
  mkdir "$RELEASE_DIR"

  if [ $? != 0 ]; then
    echo "ERROR: Failed to create release directory $RELEASE_DIR!"
    exit 1
  fi
fi

# FIND VERSION
VERSION=`grep -m 1 -oP 'version = \"\K([\d\.]+)(?=.)' $MANIFEST`

if [ -z "$VERSION" ]; then
  echo "ERROR: Failed to grep version from $MANIFEST!"
  exit 1
fi

echo "Version $VERSION detected..."

# BUILD ARCHIVE
RELEASE_NAME="tasky-time-$VERSION"
ARCHIVE="$RELEASE_DIR/$RELEASE_NAME.tar.xz"

echo "Create archive $ARCHIVE..."

# CREATE TEMPORARY DIRECTORY
ARCHIVE_DIR="$RELEASE_DIR/$RELEASE_NAME"

mkdir "$ARCHIVE_DIR"

if [ $? != 0 ]; then
  echo "ERROR: Failed to create archive directory $ARCHIVE_DIR!"
  exit 1
fi

# COPY FILES TO TEMPORARY DIRECTORY
cp "$BINARY" "$ARCHIVE_DIR"
cp -r "$LOCALE_DIR" "$ARCHIVE_DIR"

# CREATE ARCHIVE
tar -cJf "$ARCHIVE" -C "$ARCHIVE_DIR" .

if [ $? != 0 ]; then
  echo "ERROR: Failed to create archive $ARCHIVE!"
  exit 1
fi

# CLEAN UP
echo "Remove temporary files at $ARCHIVE_DIR..."

rm -dr "$ARCHIVE_DIR"

if [ $? != 0 ]; then
  echo "ERROR: Failed to remove temporary files at $ARCHIVE_DIR!"
  exit 1
fi

echo "Release Archive $ARCHIVE created!"