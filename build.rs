use std::fs;
use std::process::Command;

fn main() {
  mo_build();
}

// Build locale files
fn mo_build() {
  for lang in fs::read_dir("locale/").unwrap() {
    let mut lang = lang.unwrap().path();
    println!("Build locale {:?}", lang);

    if !lang.is_dir() {
      continue;
    }

    let mut lc_messages = lang;
    lc_messages.push("LC_MESSAGES");

    for domain in fs::read_dir(lc_messages).unwrap() {
      let domain = domain.unwrap().path();

      if domain.is_dir() {
        continue;
      }

      let extension = if let Some(ext) = domain.extension() {
        ext
      } else {
        continue;
      };

      if extension != "po" {
        continue;
      }

      let mut target = domain.clone();
      target.set_extension("mo");

      let domain_str = domain.to_str().unwrap();
      let target_str = target.to_str().unwrap();

      println!("Build {}... into {}", domain_str, target_str);

      Command::new("msgfmt")
        .args(&[domain_str, "-o", target_str])
        .status()
        .unwrap();
    }
  }
}
